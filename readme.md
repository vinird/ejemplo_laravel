# Ejemplo_Laravel

## Requerimientos
* Composer
* MySQL
* PHP
* Git

## Instalación 
* Clonar el código fuente 
```
#!

git clone https://vinird@bitbucket.org/vinird/ejemplo_laravel.git
```
* Copiar el archivo .env.example a .env
* Cambiar las variables de entorno, Host, Database, User, Password.
* Crear base de datos en MySQL.

* Instalar dependencias.
```
#!
composer install
```

* Generar llave.
```
#!
php artisan key:generate
```

* Ejecutar migraciones
```
#!

php artisan migrate
```

* Sembrar la base de datos

```
#!

php artisan db:seed
```

* Activar servidor
```
#!

php artisan serve
```